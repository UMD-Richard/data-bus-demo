package demo;

import databus.DataBus;
import databus.Subscriber;

public class Snoopy implements Subscriber
{
    DataBus bus;
    Snoopy()
    {
        bus = DataBus.getDataBus();
        bus.subscribe(this,null);
    }
    @Override
    public void onEvent(Object event)
    {
        System.out.print("(");
        System.out.print(this);
        System.out.println("):  Snooped a "+event.getClass().toString()+" from "+((Event)event).sender.toString());
        System.out.println("");
    }
}
