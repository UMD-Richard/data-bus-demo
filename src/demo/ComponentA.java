package demo;

import databus.DataBus;
import databus.Subscriber;

public class ComponentA implements Subscriber
{
    private DataBus bus;
    private float val=0;
    ComponentA()
    {
        bus= DataBus.getDataBus();
        bus.subscribe(this,MsgEvent.class);
    }
    @Override
    public void onEvent(Object event) {
        if(event.getClass()==MsgEvent.class &&
                ((Event)event).sender!=this)
        {
            System.out.print("(");
            System.out.print(this);
            System.out.print("):  ");
            System.out.print("Message event received from ");
            System.out.println(((MsgEvent)event).sender.toString());
            System.out.print("\t\t\t\tMessage: ");
            System.out.println(((MsgEvent)event).getMessage());
            System.out.print("\n");
            bus.send(new MsgRcvEvent(this, ((Event)event).sender));
        }
        //Note that while A sends MsgRcvEvent's out when it gets a msg
        // event it doesn't listen for them!
    }
    public void setValue(float Value)
    {
        val=Value;
        bus.send(new MsgEvent(this,"My new value is "+val));
    }
}
