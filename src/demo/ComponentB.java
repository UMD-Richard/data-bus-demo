package demo;

import databus.DataBus;
import databus.Subscriber;

public class ComponentB implements Subscriber
{
    private DataBus bus;
    private float money=0;
    ComponentB()
    {
        bus= DataBus.getDataBus();
        bus.subscribe(this,MsgEvent.class);
        bus.subscribe(this, MsgRcvEvent.class);
    }
    @Override
    public void onEvent(Object event) {
        if(event.getClass()==MsgEvent.class &&
                ((Event)event).sender!=this)
        {
            System.out.print("(");
            System.out.print(this);
            System.out.print("):  ");
            System.out.print("Message event received from ");
            System.out.println(((MsgEvent)event).sender.toString());
            System.out.print("\t\t\t\tMessage: ");
            System.out.println(((MsgEvent)event).getMessage());
            System.out.print("\n");
            bus.send(new MsgRcvEvent(this, ((Event)event).sender));
        }
        //in this case the component listens for msg recieved events
        else if(event.getClass()==MsgRcvEvent.class&&
                ((Event)event).sender!=this &&
                ((MsgRcvEvent)event).getReplyTo()==this)
        {
            System.out.print("(");
            System.out.print(this);
            System.out.print("):  ");
            System.out.print(((MsgRcvEvent)event).sender.toString());
            System.out.println(" received my message");
            System.out.print("\n");
        }
    }
    public void giveMoney(float Amnt)
    {
        money+=Amnt;
        bus.send(new MsgEvent(this,"I just got $"+Amnt+"!"));
    }
}
