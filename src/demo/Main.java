package demo;

import java.nio.ByteBuffer;
import java.util.Scanner;

class Event
{
    Event(Object Sender)
    {
        sender = Sender;
    }
    protected Object sender;
    public Object getSender(){return sender;}
}
class MsgEvent extends Event
{

    MsgEvent(Object Sender, String Message)
    {
        super(Sender);
        this.message=Message;
    }
    private String message;
    public String getMessage(){return message;}
}
class MsgRcvEvent extends Event
{
    MsgRcvEvent(Object Sender, Object OriginalSender)
    {
        super(Sender);
        this.replyTo = OriginalSender;
    }
    private Object replyTo;
    public Object getReplyTo(){return replyTo;}

}





public class Main {

    public static void main(String[] args)
    {
        ComponentA A = new ComponentA();
        ComponentB B = new ComponentB();
        Snoopy Snoop = new Snoopy();
        System.out.println("");
        A.setValue(50);
        B.giveMoney(100.24f);
        System.out.print("Press enter key to close program...");
        try
        {
            System.in.read();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
