package databus;


import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class DataBus {

    private static DataBus INSTANCE=null;
    private HashMap<Class, LinkedList<Subscriber>> subscribers;

    private DataBus()
    {
        subscribers = new HashMap<>();
    }
    public static DataBus getDataBus()
    {
        if(INSTANCE==null)
            INSTANCE = new DataBus();
        return INSTANCE;
    }
    public void subscribe(Subscriber subscriber, Class event)
    {
        LinkedList<Subscriber> tSubs;
        if(subscribers.containsKey(event))
        {
            tSubs = subscribers.get(event);
            if(!tSubs.contains(subscriber))
                tSubs.add(subscriber);
        }
        else
        {
            tSubs= new LinkedList<>();
            tSubs.add(subscriber);
            subscribers.put(event, tSubs );
        }
    }
    public void unSubscribe(Subscriber subscriber, Class event)
    {
        LinkedList<Subscriber> tSubs;
        if(subscribers.containsKey(event))
        {
            tSubs=subscribers.get(event);
            tSubs.remove(subscriber);
        }
    }
    public void unSubscribeAll(Subscriber subscriber)
    {
        LinkedList tSubs;
        for(Map.Entry<Class, LinkedList<Subscriber>> entry : subscribers.entrySet())
        {
            tSubs= entry.getValue();
            tSubs.remove(subscriber);
        }
    }
    public void send(Object event)
    {
        LinkedList<Subscriber> tSubs;

        if(subscribers.containsKey(null))
        {
            tSubs=subscribers.get(null);
            for(Subscriber s:tSubs)
                s.onEvent(event);
        }
        if(subscribers.containsKey(event.getClass()))
        {
            tSubs =subscribers.get(event.getClass());
            for (Subscriber s:tSubs)
                s.onEvent(event);
        }
    }
}
