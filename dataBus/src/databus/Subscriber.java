package databus;

public interface Subscriber {
    public void onEvent(Object event);

}
